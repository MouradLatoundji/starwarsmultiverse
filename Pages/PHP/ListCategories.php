<?php
//Title_page
$title_page="Catalog";

//StyleSheet
$style_file="../CSS/ListCategories_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]))
header('Location:Login.php');

//Header
include("header.inc.php");
?>

<!--Content-->

<main>
    <br/><br/>

    <div class="grid-container"> 

  

        <div  class="grid-item"><a href="Catalog_Films.php" >
            <h3 style = "text-align : center">Films</h3>
            <img src="../../Pictures/Categories/Films.jpg" class="Affiche" alt="Affiche" >
            </a>
        </div>

        <div  class="grid-item"><a href="Catalog_Caractere.php">
            <h3 style = "text-align : center">People</h3>
            <img src="../../Pictures/Categories/People.jpg" class="Affiche" alt="Affiche" >
            </a>
        </div>

        <div  class="grid-item"><a href="#" >
            <h3 style = "text-align : center">Planets(Coming Soon)</h3>
            <img src="../../Pictures/Categories/Planets.jpg" class="Affiche" alt="Affiche" >
            </a>
        </div>

        <div  class="grid-item"><a href="#" >
            <h3 style = "text-align : center">Starships(Coming Soon)</h3>
            <img src="../../Pictures/Categories/Starships.jpg" class="Affiche" alt="Affiche" >
            </a>
        </div>

        <div  class="grid-item"><a href="#" >
            <h3 style = "text-align : center">Species(Coming Soon)</h3>
            <img src="../../Pictures/Categories/Species.jpg" class="Affiche" alt="Affiche" >
            </a>
        </div>

      
     
    
</div>  
</main>       


<?php 
//Footer
include("footer.inc.php");
?>

</body>