<?php
//Title_page
$title_page="Edit People";

//StyleSheet
$style_file="../CSS/add_Catalog_Style.css";

//Head
include("head.inc.php");
?>
<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || ($_SESSION["role"]<3))
header('Location:Home.php');
?>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Fill the blanks :</h3>
</div>


      <?php
      
       $people=get_People($_POST["button"]);
       $poster=get_Poster_People($_POST["button"]) ?>

    </div>       
    <form method="POST" action="action_EditCatalog.php">
    <div class="form-group">
          <label for="name">Name :</label>
          <input type="text" class="form-control" placeholder="Enter Name" id="name" name="name" required maxlength="50" value="<?php echo $people["name"];?>" readonly required>
        </div>

        <div class="form-group">
        <label for="gender">Gender : </label><br />
       <select name="gender" id="gender">
       <option value="<?php echo $people["gender"];?>"><?php if($people["gender"]=='M') echo 'Male'; elseif($people["gender"]=='F') echo 'Female' ; else echo'-'?></option>
       <option value="M">Male</option>
       <option value="F">Female</option>
       <option value="-">-</option>
       </select>
       </div>
       <br>

        <div class="form-group">
          <label for="height">Height :</label>
          <input type="number" class="form-control" id="height" name="height" value=<?php echo $people["height"];?> required>
        </div>

        <div class="form-group">
          <label for="mass">Mass :</label>
          <input type="mass" class="form-control" id="mass" name="mass" value=<?php echo $people["mass"];?>  required>
        </div>

       <div>
        <label for="homeworld">HomeWorld : </label><br />
       <select name="homeworld" id="homeworld">
       <?php  foreach ($planets_glob as $homeworld) {
         echo '<option value="'.$homeworld["id"].'">'.$homeworld["name"].'</option>';
            }  
         ?>
         <!--<option value="0">Unknown</option>-->
       </select>
       </div>
       <br>
       <div  class="form-group">
        <label for="poster">Poster : </label><br />
       <select name="poster" id="poster">
       <option value=<?php echo $poster["path"]?>><?php echo $poster["path"]?></option>
       <?php  foreach (glob("../../Pictures/People/*") as $filename) {
         echo '<option value="'.$filename.'">'.$filename.'</option>';
            }  
         ?>
         <option value="Empty"></option>
       </select>
       </div>

            <br>

        <button type="submit" class="btn btn-primary" name="button" value="People">Confirm Edition</button>

    </form>


      
      <div><br/><a href="Catalog_Caractere.php">Back</a></div>
</main>


</body>