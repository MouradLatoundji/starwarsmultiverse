
<?php
//Title_page
$title_page="Home";

//StyleSheet
$style_file="../CSS/Home_Style.css";

//Head
include("head.inc.php");
?>

<body>
<?php
//Header
include("header.inc.php");
?>

<div class="container-fluid">
    <div class="row">
    <div class="col-sm-9">
    
        <!--Synopsis-->
    <main id="main"> 
            <h1>Star Wars : First Trilogy </h1>    
        
            <?php 
            for($i=0;$i<3;$i++){
                $title_film=$film[$i]["title"];
                $episode=$film[$i]["episode"];
                $id=$film[$i]["id"];
                $poster=$poster_film[$id];
             ?>
        <section class="storyline">
            <a href="FullContent.php?id=<?php echo $i; ?>">
            <h2><?php if(!empty($title_film)) echo $title_film;?></h2>
            <div class="row">
                <div class="col-sm-2">
            <img src=<?php if(!empty($poster_film[$film[$i]["id"]])) echo $poster_film[$film[$i]["id"]]; ?> alt="<?php if(!empty($title_film)) echo $title_film;?>">
               </div>
               <div class="col-sm-10">
            <p>
               <?php if(!empty($film[$i]["opening"])) echo $film[$i]["opening"]; ?> 
            </p>
        </div>
    </div>
            </a>       
        </section>

            <?php }
            ?>
       
    </main>
</div>



    <div class="col-sm-3"> 
        <!--Account--> 
        <aside id="account">
        <div class="row">
            <div class="col-sm">
            
            <?php
            if($_SESSION["logged"]){
            echo '<h1>Welcome, '.$_SESSION["username"].'</h1>';
             ?>
            <section><a href="Settings.php">Settings</a></section>
            <section><a href="action_Logout.php">Log out</a></section>
            <?php
            }

             else{

                ?>
                <h1>My Account</h1>
                <section><a href="Login.php">Login</a></section>
                <section><a href="SignUp.php">Sign Up</a></section>
               
                <?php
             }
            ?>
            </div>
        </div>
        </aside>

        <!--News-->
        <aside id="news">
            <div class="row">
                <div class="col-sm">
            <h1>News</h1>

            <section><a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18692611.html" target="_blank">Alexandre Astier (Kaamelott) criticizes the last trilogy</a></section>
            <section><a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18689853.html" target="_blank">Star Wars 8 : Why Sam Witwer (Darth Maul in Clone Wars) didn't like the movie</a></section>
            <section><a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18690434.html" target="_blank">A fan compares first and last trilogy in one video</a></section>
            <section><a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18686446.html" target="_blank">How J.J. Abrams and Rian Johnson appropriated the legacy of the saga</a></section>
                </div>
            </div>
        </aside>
    </div>

</div>
</div>

<?php //Footer
include("footer.inc.php");
?>

</body>

