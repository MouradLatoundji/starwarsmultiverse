<?php

/***************************************************PLANETS*********************************************************************/

//get Planet
function get_Planet($name){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM planet WHERE `name` = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s', $name);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get all planet
function get_all_Planets(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM planet "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $planet[$i]=$row;
        $i=$i+1;
    }  
    return $planet;
    }
}
}
else
return false;
}

//insert planet
function insert_Planet($name,$diameter,$population){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `planet` (`id`, `name`, `diameter`, `population`) VALUES (NULL,?, ?,?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    
    $stmt->bind_param('sis',$name,$diameter,$population);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}


//Edit planet
function edit_Planet($name,$diameter,$population){


    $planet=get_Planet($name);
    $id_planet=$planet["id"];

    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE `planet` SET  `name`=?, `diameter`=?, `population`=? WHERE `id`=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $stmt->bind_param('sisi',$name,$diameter,$population,$id_planet);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

//delete planet
function delete_Planet($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `planet` WHERE `name` = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


//Get Poster planet
function get_Poster_Planet($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_planets INNER JOIN planet ON (pictures.id=planet.id)  WHERE `name`=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();
    return $row;
    }
}
}
else
return false;
}

function get_all_Posters_Planet(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_planets INNER JOIN planet ON (pictures.id=planet.id) "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    while($row=$res->fetch_assoc()){
        $i=$row["id"];
        $poster_planet[$i]=$row["path"];
    }  
    return $poster_planet;
    }
}
}
else
return false;
}

//Change Poster
function change_Poster_Planet($name,$path){
  
    $planet=get_Planet($name);
    $id_planet=$planet["id"];

    $row=get_Poster_Planet($name); 

    if(empty($row)){  
     $mysqli=Connection();
     if(!empty($mysqli)){
         if (!($stmt = $mysqli->prepare("INSERT INTO `pictures_planets` (`id_picture`, `id`,`path`) VALUES (NULL, ?, ?)")))  {
     echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
     }
     else{
     $stmt->bind_param('is',$id_planet,$path);
 
     if (!$stmt->execute()) {
         echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
         return false;
         }
     else{
       return true;  
     }
 
     }
 
     }
     else 
     return false;
 }

 else{
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE pictures_planets SET `path` = ?  WHERE `id`= ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('si',$path,$id_planet);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
} 
}



?>