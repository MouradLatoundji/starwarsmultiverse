<?php
//Title_page
$title_page="Users";

//Head
include("head.inc.php");

//Check Log
if(empty($_SESSION["logged"]) || $_SESSION["role"]==1)
header('Location:Home.php');

//Check POST

if(empty($_POST["users"]))
header('Location:ManageUser.php');

else{
    
    switch($_POST["button"]){
        case "Delete" :
    $max=sizeof($_POST["users"]);
    for($i=0;$i<$max;$i++){
        $delete=delete_User($_POST["users"][$i]);
    }

   if(!empty($delete))
   header('Location:ManageUser.php');
break;

case "Promote" :
    $max=sizeof($_POST["users"]);
    for($i=0;$i<$max;$i++){
        $promote=promote_User($_POST["users"][$i]);
    }

   if(!empty($promote))
   header('Location:ManageUser.php');
break;

case "Downgrade" :
    $max=sizeof($_POST["users"]);
    for($i=0;$i<$max;$i++){
        $downgrade=downgrade_User($_POST["users"][$i]);
    }

   if(!empty($downgrade))
   header('Location:ManageUser.php');
break;

default :
header('Location:ManageUser.php');
break;

}

}

?>