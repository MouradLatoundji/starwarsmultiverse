<!-- Page d'information sur le site --> 

<?php

//StyleSheet
$style_file="../CSS/AboutUs.css";

//Head
include("head.inc.php");
//Title_page
$title_page="News";

?>

<body>
<?php
//Header
include("header.inc.php");
?>

<div class="container-fluid">
        <main id="main"> 
            <br/>
            <h1> About Us </h1>    
            <section id="WebSite">
                <div class="row">
                <div class="col">
                        <br/>
                        <h2>The WebSite</h2>
                        <br/><br/>
                        <p> This site is intended for all Star Wars fans. It will allow you to find all the information on the universe of George Lucas.
                         It will also allow you to have a good grade...
                        </p>
                        </div>
                </div>
            </section>  

            <section id="Members">
                <div class="row">
                 <div class="col">
                        <h2>Members</h2>
                        <br/>
                        <p>
                        This site was created by <strong>Mourad Latoundji</strong> and <strong>Claire Léguillette.</strong>
                         </p>
                         </div> 
                </div>
            </section>  

            </br>
            <h1> Contact Us </h1> 
            <section id="Emails">
                <div class="row">
                <div class="col">
                        <h2>Emails</h2>
                        <p>
                           
                            You can contact us using the following information:
                                <strong>mourad.latounji@groupe-esigelec.org</strong> or <strong>leguillette.claire@groupe-esigelec.org</strong></p>


                            </div>
                            </div>
            </section>  

            <section id="Tel">
                <div class="row">
                <div class="col">
                        <h2>Tel </h2>
                        </div>
                </div>
            </section>  

            <section id="SocialNetworks">
                <div class="row">
                <div class="col">
                        <h2>Social Networks</h2>
                        </div>
                </div>
            </section>  

          <div><p id="expression">May the Force be with you !<i class="swg swg-starwars"></i></p></div>
           
        </main>
</div>

<?php //Footer
include("footer.inc.php");
?>

</body>
