<?php

/***************************************************SPECIES*********************************************************************/

//get Species
function get_Species($name){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM species WHERE `name` = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s', $name);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get all species
function get_all_Species(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM species "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $species[$i]=$row;
        $i=$i+1;
    }  
    return $species;
    }
}
}
else
return false;
}

//insert species
function insert_Species($classification,$name){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `species` (`id`,`classification`, `name`) VALUES (NULL,?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    
    $stmt->bind_param('ss',$classification,$name);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}


//Edit species
function edit_Species($classification,$name){


    $species=get_Species($name);
    $id_species=$species["id"];

    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE `species` SET `classification`=? , `name`=? WHERE `id`=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $stmt->bind_param('ss',$classification,$name);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

//delete species
function delete_Species($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `species` WHERE `name` = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


//Get Poster species
function get_Poster_Species($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_species INNER JOIN species ON (pictures.id=species.id)  WHERE `name`=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();
    return $row;
    }
}
}
else
return false;
}

function get_all_Posters_Species(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_species INNER JOIN species ON (pictures.id=species.id)"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    while($row=$res->fetch_assoc()){
        $i=$row["id"];
        $poster_species[$i]=$row["path"];
    }  
    return $poster_species;
    }
}
}
else
return false;
}

//Change Poster
function change_Poster_Species($name,$path){
  
    $species=get_Species($name);
    $id_species=$species["id"];

    $row=get_Poster_Species($name); 

    if(empty($row)){  
     $mysqli=Connection();
     if(!empty($mysqli)){
         if (!($stmt = $mysqli->prepare("INSERT INTO `pictures_species` (`id_picture`, `id`,`path`) VALUES (NULL, ?, ?)")))  {
     echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
     }
     else{
     $stmt->bind_param('is',$id_species,$path);
 
     if (!$stmt->execute()) {
         echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
         return false;
         }
     else{
       return true;  
     }
 
     }
 
     }
     else 
     return false;
 }

 else{
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE pictures_species SET `path` = ?  WHERE  `id`= ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('si',$path,$id_species);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
} 
}



?>