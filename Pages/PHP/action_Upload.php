<?php
//Title_page
$title_page="Traitment...";

//Head
include("head.inc.php");

//Check Log
if(empty($_SESSION["logged"]) || $_SESSION["role"]<3)
header('Location:Home.php');

if(isset($_POST ["button"])){
    // Tester si le fichier a été envoyé sans erreur
    if (isset($_FILES['pictures']) AND $_FILES['pictures']['error'] == 0)
    {
            // Tester la taille du fichier
            if ($_FILES['pictures']['size'] <= 1000000)
            {
                    // Tester si l'extension est autorisée
                    $infosfichier = pathinfo($_FILES['pictures']['name']);
                    $extension_upload = $infosfichier['extension'];
                    $extensions_autorisees = array('jpg', 'gif', 'png','jpeg');
                    if (in_array($extension_upload, $extensions_autorisees))
                    {
                            // stocker le fichier définitivement dans le dossier "pictures"
    
                            move_uploaded_file($_FILES['pictures']['tmp_name'], '../../Pictures/'.$_POST['category'].'/' . basename($_FILES['pictures']['name']));
                            echo "L'envoi a bien été effectué !<br>";
                            $_SESSION["error_upload"]=true;    
                        header('Location:Upload.php?error=0');
                    }
                    else{
                        echo "Erreur :Extension non autorisée.<br>";
                        $_SESSION["error_upload"]=true;    
                        header('Location:Upload.php?error=1');
                    }
            }
            else{
                echo "le fichier est trop gros<br>";
                $_SESSION["error_upload"]=true;    
                header('Location:Upload.php?error=2');
            }
    
       }
       else{
           echo "Erreur  d'envoi<br>";
           $_SESSION["error_upload"]=true;    
            header('Location:Upload.php?error=3');
       }
    }

     //Source : www.exelib.net
    ?>
   