<?php
//Title_page
$title_page="Sign Up";

//StyleSheet
$style_file="../CSS/SignUp_Style.css";

//Head
include("head.inc.php");
?>

<body>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Please fill out this form :</h3>
</div>
<div class="col-sm">
  <?php
     
     if(!empty($_SESSION["error_sign_up"])){
       if(isset($_GET['error'])){
         if($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Username already used !</strong></span>';
         elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Email already used !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_sign_up"]);
      }
            ?>
      </div>
    </div>       
    <form method="POST" action="action_Sign_Up.php">
        <div class="form-group">
          <label for="first_name">First Name :</label>
          <input type="text" class="form-control" placeholder="Enter First Name" id="first_name" name="first_name" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="last_name">Last Name :</label>
          <input type="text" class="form-control" placeholder="Enter Last Name" id="last_name" name="last_name" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="birthday">Birthday :</label>
          <input type="date" class="form-control" placeholder="Enter Birthday" id="birthday" name="birthday" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="username">Username :</label>
          <input type="text" class="form-control" placeholder="Enter Username" id="username" name="username" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="email">Email :</label>
          <input type="email" class="form-control" placeholder="Enter Email" id="email" name="email" required maxlength="60" required>
        </div>

        <div class="form-group">
          <label for="password">Password:</label>
          <input type="password" class="form-control" placeholder="Enter password" id="password" name="password" required maxlength="30" required>
        </div>

        <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="checkbox"> Remember me
          </label>
        </div>

       <!-- <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="checkbox"> Remember me
          </label>
        </div>-->
        <button type="submit" class="btn btn-primary" name="button">Sign Up</button>
      </form>
      <div><br/><a href="Login.php">Already User? Login</a></div>
      <div><br/><a href="Home.php">Return Home</a></div>
</main>


</body>