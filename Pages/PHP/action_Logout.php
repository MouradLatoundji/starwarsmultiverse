<?php 
session_start(); 

require('functions.inc.php');

//Destroy Session
if(!empty($_SESSION["logged"]))
session_destroy();

//Destroy cookies
if(!empty($_COOKIE["id_user"])){
    delete_all_cookies();
}

header('Location:Home.php');
?>