<?php

function Connection(){

require('param.inc.php');
$mysqli = new mysqli($host,$login,$passwd,$dbname);
if ($mysqli->connect_errno) {
echo "Connect Error: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
return false;
}

else
return $mysqli;
}

/*User*/
function get_User($login){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM user WHERE username = ? OR email = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('ss', $login,$login);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    //mysqli_close($mysqli);
    return $row;
    }
}
}

else
return false;
}

//Get All Users
function get_all_Users(){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM user ORDER BY `role` DESC"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
        while($row=$res->fetch_assoc()){
            $users[$i]=$row;
            $i=$i+1;
    }

    return $users;
}
}
}

else
return false;

}

function get_all_Users_by($username){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM user WHERE username LIKE CONCAT(?,'%') ORDER BY `role` DESC"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$username);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
        while($row=$res->fetch_assoc()){
            $users[$i]=$row;
            $i=$i+1;
    }

    return $users;
}
}
}

else
return false;

}

//edit User
function edit_User($id_user,$username,$first_name,$last_name,$birthday){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE user SET username=?, first_name = ?, last_name=?,birthday=? WHERE id_user=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $birthdaySQL = date('Y-m-d',strtotime($birthday));
    $stmt->bind_param('ssssi',$username,$first_name,$last_name,$birthdaySQL,$id_user);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;

}

function delete_User($username){
$mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `user` WHERE username = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$username);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


function promote_User($username){
    $mysqli=Connection();
        if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE user SET `role` = 2 WHERE username=?"))) 
        {
        echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
        }
    
        $stmt->bind_param('s',$username);
    
    
        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
            }
        
        else{
          return true;  
    }
    }
    
    else
    return false;
    }

    function downgrade_User($username){
        $mysqli=Connection();
            if(!empty($mysqli)){
            if (!($stmt = $mysqli->prepare("UPDATE user SET `role` = 1 WHERE username=?"))) 
            {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
            }
        
            $stmt->bind_param('s',$username);
        
        
            if (!$stmt->execute()) {
                echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
                return false;
                }
            
            else{
              return true;  
        }
        }
        
        else
        return false;
        }

/*Connection*/
function connect_User($login,$password){
$row=get_User($login);
if(empty($row))
return NULL;

else{

$passcrypt=$row["password"];

if(!empty($passcrypt)){
$check=password_verify($password,$passcrypt);
if(empty($check))
return -1;
else{
if(!empty($row))
return $row;
else
return NULL;
}
}
else
return NULL;
}
}

/*Insert User*/
function insert_User($first_name,$last_name,$birthday,$username,$email,$password,$role){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `user` (`id_user`, `first_name`, `last_name`,`birthday`, `username`, `email`, `password`, `role`) VALUES (NULL,?, ?, ?,?, ?,?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    $passcrypt = password_hash($password, PASSWORD_BCRYPT);
    $birthdaySQL = date('Y-m-d',strtotime($birthday));
    $stmt->bind_param('ssssssi',$first_name,$last_name,$birthdaySQL,$username, $email,$passcrypt,$role);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

function delete_all_cookies(){
     
    if(!empty($_COOKIE["id_user"])){
        unset($_COOKIE["id_user"]);
        setcookie ("id_user",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["first_name"])){  
        unset($_COOKIE["first_name"]);
        setcookie ("first_name",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["last_name"])){
        unset($_COOKIE["last_name"]);
        setcookie ("last_name",NULL, time() - 3600);
    }    

    if(!empty($_COOKIE["birthday"])){
        unset($_COOKIE["birthday"]);
        setcookie ("birthday",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["username"])){
        unset($_COOKIE["username"]);
        setcookie ("username",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["email"])){
        unset($_COOKIE["email"]);
        setcookie ("email",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["password"])){
        unset($_COOKIE["password"]);
        setcookie ("password",NULL, time() - 3600);
    }

    if(!empty($_COOKIE["role"])){
        unset($_COOKIE["role"]);
        setcookie ("role",NULL, time() - 3600);
    }    
}

//Get Reviews
function get_all_Reviews($id_film){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM reviews INNER JOIN user ON (user.id_user=reviews.id_user) WHERE id_film=?  ORDER BY `date` DESC"))) //LIMIT 0,30
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i', $id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $reviews[$i]=$row;
        $i=$i+1;
    }  
    return $reviews;
    }
}
}
else
return false;
}

function get_Reviews($id_user,$id_film){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM reviews WHERE id_user=? AND id_film=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('ii', $id_user,$id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc(); 
    return $row;
    }
}
}
else
return false;
}

//Insert reviews
function insert_Reviews($id_user,$id_film,$review,$comment,$date){

   $row=get_Reviews($id_user,$id_film); 

   if(empty($row)){
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `reviews` (`id_user`, `id_film`, `review`,`comment`, `date`) VALUES (?, ?, ?,?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    $stmt->bind_param('iisss',$id_user,$id_film,$review,$comment,$date);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }
}

else{
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE reviews SET review = ?, comment=?, `date`=? WHERE id_user = ? AND id_film=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    $stmt->bind_param('sssii',$review,$comment,$date,$id_user,$id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    } 
   
}
    
}

//Count reviews
function get_Count_Reviews_Film($id_film){
    
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT count(*) AS 'count_reviews' FROM reviews WHERE id_film = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i', $id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

function get_Count_Reviews($id_film,$review){
    
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT count(*) AS 'count_reviews' FROM reviews WHERE id_film = ? AND review=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('is', $id_film,$review);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}


function change_Password($username,$old_password,$new_password){
    $row=get_User($username);

    if(empty($row))
    return NULL;
    
    else{
    
    $passcrypt=$row["password"];
    
    if(!empty($passcrypt)){
    $check=password_verify($old_password,$passcrypt);
    if(empty($check))
    return -1;
    else{
    //SET UP
    $mysqli=Connection();
    if (!($stmt = $mysqli->prepare("UPDATE user SET  `password`= ? WHERE username = ?")))  {
        echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        }
        else{
        $passcrypt2 = password_hash($new_password, PASSWORD_BCRYPT);   
        $stmt->bind_param('ss',$passcrypt2,$username);
    
        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
            }
        else{
          return 1;  
        }
    
        }
    }
    }
    else
    return NULL;
}
}


include("functions_Films.inc.php");
include("functions_People.inc.php");
include("functions_Planets.inc.php");
include("functions_Species.inc.php");
include("functions_Starships.inc.php");



?>