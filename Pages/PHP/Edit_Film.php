<?php
//Title_page
$title_page="Edit Film";

//StyleSheet
$style_file="../CSS/add_Catalog_Style.css";

//Head
include("head.inc.php");
?>
<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || ($_SESSION["role"]<3))
header('Location:Home.php');
?>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Fill the blanks :</h3>
</div>


      <?php $film=get_Film($_POST["button"]);
            $poster=get_Poster_Film($_POST["button"]) ?>

    </div>       
    <form method="POST" action="action_EditCatalog.php">
    <div class="form-group">
          <label for="title">Title :</label>
          <input type="text" class="form-control" placeholder="Enter Title" id="title" name="title" required maxlength="30" value="<?php echo $film["title"];?>" required>
        </div>

        <div class="form-group">
          <label for="release_date">Release Date :</label>
          <input type="date" class="form-control" placeholder="Enter Release Date" id="release_date" name="release_date" required maxlength="30" value=<?php echo $film["release_date"];?> required>
        </div>

        <div class="form-group">
          <label for="episode">Episode :</label>
          <input type="number" class="form-control" id="episode" name="episode" value=<?php echo $film["episode"];?> readonly required >
        </div>

        <div class="form-group">
          <label for="opening">Opening :</label>
          <textarea class="form-control" name="opening" id="opening" rows="10" cols="80" required><?php echo $film["opening"];?></textarea>
        </div>

       <div class="form-group">
        <label for="poster">Poster : </label><br />
       <select name="poster" id="poster">
       <option value=<?php echo $poster["path"]?>><?php echo $poster["path"]?></option>
       <?php  foreach (glob("../../Pictures/Films/*") as $filename) {
         echo '<option value="'.$filename.'">'.$filename.'</option>';
            }  
         ?>
         <option value="Empty"></option>
       </select>
       </div>

            <br>

        <button type="submit" class="btn btn-primary" name="button" value="Film">Confirm Edition</button>

    </form>


      
      <div><br/><a href="Catalog_Films.php">Back</a></div>
</main>


</body>