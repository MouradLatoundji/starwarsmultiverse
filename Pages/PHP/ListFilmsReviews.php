<!-- Page liste des films avec commentaires --> 

<?php
//Title_page
$title_page="Reviews";

//StyleSheet
$style_file="../CSS/ListFilmsReviews_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]))
header('Location:Login.php');

//Header
include("header.inc.php");
?>

<!--Content-->

<main>
    <h1>Talk about your Favorite Star Wars !</h1>
<br/><br/>

    <div class="grid-container"> 

    <?php

        $max=sizeof($film);

        for($i=0;$i<$max;$i++){
            $title_film=$film[$i]["title"];
            $episode=$film[$i]["episode"];
            $id=$film[$i]["id"];
            $poster=$poster_film[$id];
    ?>

        <div  class="grid-item"><a href="Reviews.php?film=<?php echo $episode;?>" >
            <h3 style="text-align : center"><?php echo $title_film;?></h3>
            <img src=<?php if(!empty($poster)) echo $poster; ?> class="Affiche" alt="Affiche" >
            </a>
        </div>

      <?php
        }
      
      ?>

     
    
</div>  
</main>       


<?php 
//Footer
include("footer.inc.php");
?>

</body>