<?php
//Title_page
$title_page="Settings";

//StyleSheet
$style_file="../CSS/ChangeProfile_Style.css";

//Head
include("head.inc.php");
?>
<body>

<?php
//Check Log
if(empty($_SESSION["logged"]))
header('Location:Home.php');
?>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Edit your informations :</h3>
</div>
<div class="col-sm">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_edit"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Username already used !</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_edit"]);
      }
            ?>

      </div>
    </div>       
    <form method="POST" action="action_ChangeProfile.php">
    <div class="form-group">
          <label for="username">Username :</label>
          <input type="text" class="form-control" placeholder="Enter Username" id="username" name="username" required maxlength="30" value=<?php echo $_SESSION["username"]; ?> required>
        </div>

        <div class="form-group">
          <label for="first_name">First Name :</label>
          <input type="text" class="form-control" placeholder="Enter First Name" id="first_name" name="first_name" required maxlength="30" value=<?php echo $_SESSION["first_name"]; ?> required>
        </div>

        <div class="form-group">
          <label for="last_name">Last Name :</label>
          <input type="text" class="form-control" placeholder="Enter Last Name" id="last_name" name="last_name" required maxlength="30" value=<?php echo $_SESSION["last_name"]; ?> required>
        </div>

        <div class="form-group">
          <label for="birthday">Birthday :</label>
          <input type="date" class="form-control" placeholder="Enter Birthday" id="birthday" name="birthday" required maxlength="30" value=<?php echo $_SESSION["birthday"]; ?> required>
        </div>

        <button type="submit" class="btn btn-primary" name="button" value="edit">Edit</button>

</form>

<br>
<div class="row">
    <div class="col-sm">
    <h3>Change your Password :</h3>
</div>
<div class="col-sm">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_password"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Wrong Password !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error !</strong></span>';     
       }

       unset($_SESSION["error_password"]);
      }
            ?>
      </div>
      </div>

    <form method="POST" action="action_ChangeProfile.php">
        <div class="form-group">
          <label for="old_password">Old Password:</label>
          <input type="password" class="form-control" placeholder="Enter old password" id="old_password" name="old_password" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="new_password">New Password:</label>
          <input type="password" class="form-control" placeholder="Enter new password" id="new_password" name="new_password" required maxlength="30" required>
        </div>

        <div>
        <button type="submit" class="btn btn-primary" name="button" value="change_Password">Change My Password</button>
        </div>
        </form>
<br>


<form method="POST" action="action_ChangeProfile.php">
        <div>
        <button type="submit" class="btn btn-danger" name="button" value="delete_Account">Delete my account</button>
        <span class="alert alert-warning"><strong>Warning ! Your account will definitely be deleted</strong></span>
        </div>

      </form>
      
      <div><br/><a href="Settings.php">Back</a></div>
</main>


</body>