<?php
//Title_page
$title_page="Login";

//StyleSheet
$style_file="../CSS/Login_Style.css";

//Head
include("head.inc.php");
?>

<body>

<main id="form">
<div class="row">
    <div class="col-sm">
  <h3>Enter your credentials :</h3>
</div>
<div class="col-sm">
  <?php
     

     if(!empty($_SESSION["error_login"])){
       if(isset($_GET['error'])){
         if($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Wrong Username or Email !</strong></span>';
         elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Wrong Password !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Wrong !</strong></span>';     
       }

       unset($_SESSION["error_login"]);
      }
            ?>
        </div>
    </div>
      
    <form method="POST" action="action_Login.php">

        <div class="form-group">
          <label class="credential" for="username_email">Username or Email:</label>
          <input type="text" class="form-control" placeholder="Enter Username or Email" id="username_email" name="username_email" required>
        </div>

        <div class="form-group">
          <label  class="credential" for="password">Password:</label>
          <input type="password" class="form-control" placeholder="Enter password" id="password" name="password" required>
        </div>

        <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="checkbox"> Remember me
          </label>
        </div>

        <button type="submit" class="btn btn-primary" name="button">Login</button>
      </form>
      <div><br/><a href="SignUp.php">New User? Sign Up</a></div>
      <div><br/><a href="Home.php">Return Home</a></div>
 </main>


</body>