<?php

function delete_People_PlaysIn($film_name){

    $film=get_Film($film_name);
    $id_film=$film["id"];

    $mysqli=Connection();
        if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("DELETE FROM `playsin` WHERE id_film = ?"))) 
        {
        echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
        }
    
        $stmt->bind_param('i',$id_film);
    
    
        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
            }
        
        else{
          return true;  
    }
    }
    
    else
    return false;
    }

    function delete_Films_PlaysIn($people_name){

        $people=get_People($people_name);
        $id_people=$people["id"];
    
        $mysqli=Connection();
            if(!empty($mysqli)){
            if (!($stmt = $mysqli->prepare("DELETE FROM `playsin` WHERE id_people = ?"))) 
            {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
            }
        
            $stmt->bind_param('i',$id_people);
        
        
            if (!$stmt->execute()) {
                echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
                return false;
                }
            
            else{
              return true;  
        }
        }
        
        else
        return false;
        }

    function insert_PlaysIn($film_name,$people_name){

        $film=get_Film($film_name);
        $id_film=$film["id"];
        $people=get_People($people_name);
        $id_people=$people["id"];

        $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `film` (`id_people`, `id_film`) VALUES (?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $stmt->bind_param('ii',$id_people,$id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }


    }
    

?>