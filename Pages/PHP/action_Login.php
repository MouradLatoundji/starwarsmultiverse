<?php 
//Title_page
$title_page="Treatment...";

//Head
include("head.inc.php");


$username_email = htmlentities( $_POST['username_email']);
$password = htmlentities( $_POST['password']);


$connection=connect_User($username_email,$password);
  if(empty($connection)){
         $_SESSION["error_login"]=true;
         header('Location:Login.php?error=1');
    }   
    
    elseif ($connection==-1){
        $_SESSION["error_login"]=true;
        header('Location:Login.php?error=2');
        }
    else
    {
            $row=$connection;

            //Session
            $_SESSION["logged"]=true;   
            $_SESSION["id_user"]=$row["id_user"]; 
            $_SESSION["first_name"]=$row["first_name"];    
            $_SESSION["last_name"]=$row["last_name"];
            $_SESSION["birthday"]=$row["birthday"]; 
            $_SESSION["username"]=$row["username"]; 
            $_SESSION["email"]=$row["email"]; 
            $_SESSION["password"]=$row["password"];  
            $_SESSION["role"]=$row["role"];  
            
            //Cookies
            if(isset($_POST['checkbox'])) {
                setcookie('id_user',$row["id_user"],time()+365*24*3600,null,null,false,true);
                setcookie('first_name',$row["first_name"],time()+365*24*3600,null,null,false,true);
                setcookie('last_name',$row["last_name"],time()+365*24*3600,null,null,false,true);
                setcookie('birthday',$row["birthday"],time()+365*24*3600,null,null,false,true);
                setcookie('username',$row["username"],time()+365*24*3600,null,null,false,true);
                setcookie('email',$row["email"],time()+365*24*3600,null,null,false,true);
                setcookie('password',$row["password"],time()+365*24*3600,null,null,false,true);
                setcookie('role',$row["role"],time()+365*24*3600,null,null,false,true);
             }
           
            header('Location:Home.php');
     }
    


?>
