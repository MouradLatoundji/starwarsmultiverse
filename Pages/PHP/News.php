<!-- Page de listing des news --> 

<?php

//StyleSheet
$style_file="../CSS/News.css";

//Head
include("head.inc.php");
//Title_page
$title_page="News";

include("functions_News.inc.php");
?>

<body>
<?php
//Header
include("header.inc.php");
?>

<div class="container-fluid">
    <div class="row">
        <main id="main"> 
        <br/>
                <h1>Star Wars : What's up ? </h1>    
            
            <section class="News">
                <a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18692611.html">
                <div class="row">
                    <div class="col-sm-2">
                        <?php $test=get_Picture_News(1);?>
                        <img src="<?php if(!empty($test)) echo $test; ?>" alt="Article1" style="width: 100%">
                    </div>
                    <div class="col-sm-10">
                        <br/><br/>
                        <h2>Alexandre Astier (Kaamelott) criticizes the last trilogy</h2>
                    </div>
                </div>
                </a>       
            </section>

            <section class="News">
                <a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18689853.html" target="_blank">
                <div class="row">
                    <div class="col-sm-2">
                        <?php $test=get_Picture_News(2);?>
                        <img src=<?php if(!empty($test)) echo $test; ?> alt="Article2" style="width: 100%">
                    </div>
                    <div class="col-sm-10">
                        <br/><br/>
                        <h2>Star Wars 8 : Why Sam Witwer (Darth Maul in Clone Wars) didn't like the movie</h2>
                    </div>
                </div>
                </a>
            </section>

            <section class="News">
                <a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18690434.html" target="_blank">
                <div class="row">
                    <div class="col-sm-2">
                        <?php $test=get_Picture_News(4);?>
                        <img src=<?php if(!empty($test)) echo $test; ?> alt="Article3" style="width: 100%">
                    </div>

                    <div class="col-sm-10">
                        <h2>A fan compares first and last trilogy in one video</h2>
                    </div>
                </div>
                </a>
            </section>

            <section class="News">
                <a href="https://www.allocine.fr/article/fichearticle_gen_carticle=18686446.html" target="_blank">
                <div class="row">
                    <div class="col-sm-2">
                        <?php $test=get_Picture_News(3);?>
                        <img src=<?php if(!empty($test)) echo $test; ?> alt="Article4" style="width: 100%">
                    </div>

                    <div class="col-sm-10">
                        <h2>How J.J. Abrams and Rian Johnson appropriated the legacy of the saga</h2>
                    </div>
                </div>    
                </a>       
            </section>

        </main>
    </div>    
</div>

<?php //Footer
include("footer.inc.php");
?>

</body>
