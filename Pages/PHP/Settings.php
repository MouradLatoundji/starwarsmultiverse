<?php
//Title_page
$title_page="Settings";

//StyleSheet
$style_file="../CSS/Settings_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]))
header('Location:Home.php');
?>

<!--Content-->
<main>
<div class="container-fluid">
<h1>My Profile</h1>

<div class="row">
 <label>Username : </label>
  <?php if(!empty($_SESSION["username"])) echo '<span>'.$_SESSION["username"].'</span>'; ?>
</div>

<div class="row">
 <label>First Name : </label>
  <?php if(!empty($_SESSION["first_name"])) echo '<span>'.$_SESSION["first_name"].'</span>'; ?>
</div>

<div class="row">
 <label>Last Name : </label>
  <?php if(!empty($_SESSION["last_name"])) echo '<span>'.$_SESSION["last_name"].'</span>'; ?>
</div>

<div class="row">
 <label>Birthday : </label>
  <?php if(!empty($_SESSION["birthday"])) echo '<span>'.$_SESSION["birthday"].'</span>'; ?>
</div>

<div class="row">
 <label>Email : </label>
  <?php if(!empty($_SESSION["email"])) echo '<span>'.$_SESSION["email"].'</span>'; ?>
</div>

<div class="row">
  <?php 
  if(!empty($_SESSION["role"]) && $_SESSION["role"]==1) 
    $role_str="User";
  elseif(!empty($_SESSION["role"]) && $_SESSION["role"]==2)
    $role_str="Moderator";
  elseif (!empty($_SESSION["role"]) && $_SESSION["role"]==3) {
    $role_str="Administrator";
  }
    if(isset($role_str))
    echo '<p>You are <strong>'.$role_str.'</strong></p>'; 
    ?>
</div>

<div class="row">
<a href="ChangeProfile.php">Edit my Profile</a>
</div>

<?php
if(!empty($_SESSION["role"]) && ($_SESSION["role"]>=2))
{
?>

<div class="row">
<a href="ManageUser.php">Manage the Users</a>
</div>

<?php
}

if(!empty($_SESSION["role"]) && ($_SESSION["role"]>=3))
{
?>

<div class="row">
<!---<a href="ListCategories.php">Manage the Catalog</a>-->
<a href="ListCategories.php">Manage the Catalog</a>
</div>

<div class="row">
<a href="Upload.php">Add More Pictures</a>
</div>

<?php
}
?>
</div>

<div><br/><a href="Home.php">Return Home</a></div>
</main>

</body>