<?php 
//Title_page
$title_page="Treatment...";

//Head
include("head.inc.php");


$first_name= htmlentities( $_POST['first_name']);
$last_name= htmlentities( $_POST['last_name']);
$birthday= htmlentities( $_POST['birthday']);
$username= htmlentities( $_POST['username']);
$email= htmlentities( $_POST['email']);
$password = htmlentities( $_POST['password']);
$role=1;


 /*Check if already exists*/
 $get_Username=get_User($username);
 $get_Email=get_User($email);
 if(!empty($get_Username)){
    $_SESSION["error_sign_up"]=true;    
    header('Location:SignUp.php?error=1');
 }
 elseif(!empty($get_Email)){
    $_SESSION["error_sign_up"]=true; 
    header('Location:SignUp.php?error=2');
 }

else{
$insert=insert_User($first_name,$last_name,$birthday,$username,$email,$password,$role);
    
    if(!empty($insert)){
        $connection=connect_User($username,$password);
  if(empty($connection)){
         header('Location:Login.php');
    }   
    
    elseif ($connection==-1){
        header('Location:Login.php');
        }
    else
    {
            $row=$connection;

            //Session
            $_SESSION["logged"]=true;   
            $_SESSION["id_user"]=$row["id_user"]; 
            $_SESSION["first_name"]=$row["first_name"];    
            $_SESSION["last_name"]=$row["last_name"];
            $_SESSION["birthday"]=$row["birthday"]; 
            $_SESSION["username"]=$row["username"]; 
            $_SESSION["email"]=$row["email"]; 
            $_SESSION["password"]=$row["password"];  
            $_SESSION["role"]=$row["role"];  
            
            //Cookies
            if(isset($_POST['checkbox'])) {
                setcookie('id_user',$row["id_user"],time()+365*24*3600,null,null,false,true);
                setcookie('first_name',$row["first_name"],time()+365*24*3600,null,null,false,true);
                setcookie('last_name',$row["last_name"],time()+365*24*3600,null,null,false,true);
                setcookie('birthday',$row["birthday"],time()+365*24*3600,null,null,false,true);
                setcookie('username',$row["username"],time()+365*24*3600,null,null,false,true);
                setcookie('email',$row["email"],time()+365*24*3600,null,null,false,true);
                setcookie('password',$row["password"],time()+365*24*3600,null,null,false,true);
                setcookie('role',$row["role"],time()+365*24*3600,null,null,false,true);
             }
           
            header('Location:Home.php');
     }
    }
    else{    
        header('Location:Sign.php?error=3');
    }

}


?>
