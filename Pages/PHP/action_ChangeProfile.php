<?php
//Title_page
$title_page="Users";

//Head
include("head.inc.php");

//Check Log
if(empty($_SESSION["logged"]))
header('Location:Home.php');

//Check POST

if(empty($_POST))
header('Location:ChangeProfile.php');

else{
    
switch($_POST["button"]){
case "edit" :
    $id_user= $_SESSION["id_user"];
    $username=htmlentities( $_POST['username']);
    $first_name=htmlentities( $_POST['first_name']);
    $last_name=htmlentities( $_POST['last_name']);
    $birthday=htmlentities( $_POST['birthday']);

    //Check Username Duplication
    if($username!=$_SESSION["username"]){
    $get_Username=get_User($username);
   // $get_Username=htmlentities($get_Username);
    if(!empty($get_Username)){
    $_SESSION["error_edit"]=true;    
    header('Location:ChangeProfile.php?error=1');
     }
    }

    if(empty($_SESSION["error_edit"])){
        $edit=edit_User($id_user,$username,$first_name,$last_name,$birthday);
        if(empty($edit)){
            $_SESSION["error_edit"]=true;    
            header('Location:ChangeProfile.php?error=2'); 
        }
        else{
            $row=get_User($username);
            $_SESSION["first_name"]=$row["first_name"];    
            $_SESSION["last_name"]=$row["last_name"];
            $_SESSION["birthday"]=$row["birthday"]; 
            $_SESSION["username"]=$row["username"];
            $_SESSION["error_edit"]=true;
            header('Location:ChangeProfile.php?error=0');
        }
    }

        

 
break;

case "change_Password" :
   
    $username= $_SESSION["username"];
    $old_password=htmlentities( $_POST['old_password']);
    $new_password=htmlentities( $_POST['new_password']);

    $change=change_Password($username,$old_password,$new_password);
    if(empty($change)){
        $_SESSION["error_password"]=true;    
        header('Location:ChangeProfile.php?error=1'); 
    }
    elseif($change==-1){
        $_SESSION["error_password"]=true;    
        header('Location:ChangeProfile.php?error=2');
    }
    elseif($change==1){
        $row=get_User($username);
        $_SESSION["password"]=$row["password"];
        $_SESSION["error_password"]=true;
        header('Location:ChangeProfile.php?error=0');
    }



break;

case "delete_Account" :

    $username= $_SESSION["username"];
    $delete=delete_User($username);

    if(!empty($delete)){
        header('Location: action_Logout.php'); 
    }
break;

default :
header('Location:ChangeProfile.php');
break;

}

}

?>