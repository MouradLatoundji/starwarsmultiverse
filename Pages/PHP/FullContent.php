<!--Page de listing de tous les films-->

<?php
//Title_page
$title_page="Catalog";

//StyleSheet
$style_file="../CSS/Style.inc.css";
$style_file="../CSS/FullContent.css";

//Head
include("head.inc.php");
include("film_funtion.inc.php");
?>


<body>

<?php
    //Header
    include("header.inc.php");

    //Check Log
    if(empty($_SESSION["logged"]))
    header('Location:Login.php');   
    ?>

<main>
<div class="container-fluid"> 
        <div class="row">
        
            <div  class="col-sm-3">
                <br/>
                <br/>
                <!-- Récupération de l'image dans depuis la BDD et affichage --> 
                        <img src=<?php $id=$film[$_GET['id']]["id"]; if(!empty($poster_film[$id])) echo $poster_film[$id]; ?> id="AfficheI" alt="AfficheI" style="width: 100%">            
            </div>

           
            <div  class="col-sm-9">
                <div class="info">
                <?php 
               $id_film=get_id_Film($film[$_GET['id']]["episode"]);
                $film=get_information_Films($id_film);
                if(!empty($film)){
                    $title=$film["title"];
                    $date=$film["release_date"];
                    $episode=$film["episode"];
                    $opening=$film["opening"];
                }
                else{
                    $title="";
                    $date="";
                    $episode="";
                }
                 ?>
                <h2><?php echo $title?></h2>
                <p><?php echo "Release date:" .$date?></p>
                <p><?php echo "Episode : " .$episode?></p>
                <p><?php echo "Opening" .$opening?></p>
                </div>

            <div class="Peoples">
            <p><?php
            echo  "Character in " .$title. " : <br />";
            $peoples=get_Film_Actor($id_film);
            $max=sizeof($peoples);

            for($i=0;$i<$max;$i++){
                $name=$peoples[$i]['name'];
                echo $name . "<br />";
            }
            ?></p>
            </div>
        </div>                 
    </div>
    </main> 
    <br/><br/>


    <?php 
    //Footer
    include("footer.inc.php");
    ?>

  


</body>