<?php 
session_start(); 

if(!empty($_COOKIE["id_user"])){
    $_SESSION["logged"]=true;   
    $_SESSION["id_user"]=$_COOKIE["id_user"]; 
    $_SESSION["first_name"]=$_COOKIE["first_name"];    
    $_SESSION["last_name"]=$_COOKIE["last_name"]; 
    $_SESSION["birthday"]=$_COOKIE["birthday"]; 
    $_SESSION["username"]=$_COOKIE["username"]; 
    $_SESSION["email"]=$_COOKIE["email"]; 
    $_SESSION["password"]=$_COOKIE["password"];  
    $_SESSION["role"]=$_COOKIE["role"];
}


if(empty($_SESSION["logged"]))
 $_SESSION["logged"]=false;

 if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('Europe/Paris');
}

require('functions.inc.php');
include('required.inc.php');
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--Style-->

    <link rel="stylesheet" href="../CSS/Style.inc.css">
    <link rel="stylesheet" href="../../Pictures/Icones/css/starwars-glyphicons.css">
    <?php 
    if(isset($style_file)) 
    echo "<link rel=\"stylesheet\" href=$style_file>" ;
    ?>
    <link href="../BootStrap/css/bootstrap.min.css" rel="stylesheet">

    <!--Title-->
    <title>
        <?php 
        if(isset($title_page))
    echo $title_page ; 
        else
    echo "Star Wars MultiVerse"  ;  
    ?>   
    </title>
</head>