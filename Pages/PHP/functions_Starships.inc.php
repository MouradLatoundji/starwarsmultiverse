<?php

/***************************************************STARSHIPS*********************************************************************/

//get Starships
function get_Starships($class){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM starship WHERE `class` = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s', $class);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get all Starships
function get_all_Starships(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM starship "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $starship[$i]=$row;
        $i=$i+1;
    }  
    return $starship;
    }
}
}
else
return false;
}

//insert Starships
function insert_Starships($class,$mglt){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `starship` (`id`,`class`, `mglt`) VALUES (NULL,?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    
    $stmt->bind_param('si',$class,$mglt);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}


//Edit starship
function edit_Starships($class,$mglt){


    $starship=get_Starships($class);
    $id_starship=$starship["id"];

    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE `starship` SET `class`=? , `mglt`=? WHERE `id`=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $stmt->bind_param('sii',$class,$mglt,$id_starship);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

//delete starship
function delete_Starships($class){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `starship` WHERE `class` = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$class);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


//Get Poster starship
function get_Poster_Starships($class){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_starships INNER JOIN starship ON (pictures.id=starship.id)  WHERE `class`=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$class);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();
    return $row;
    }
}
}
else
return false;
}

function get_all_Posters_Starships(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_starships INNER JOIN starship ON (pictures.id=starship.id)"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    while($row=$res->fetch_assoc()){
        $i=$row["id"];
        $poster_starship[$i]=$row["path"];
    }  
    return $poster_starship;
    }
}
}
else
return false;
}

//Change Poster
function change_Poster_Starships($class,$path){
  
    $starship=get_Starship($class);
    $id_starship=$starship["id"];

    $row=get_Poster_Starships($class); 

    if(empty($row)){  
     $mysqli=Connection();
     if(!empty($mysqli)){
         if (!($stmt = $mysqli->prepare("INSERT INTO `pictures_starships` (`id_picture`, `id`,`path`) VALUES (NULL, ?, ?)")))  {
     echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
     }
     else{
     $stmt->bind_param('is',$id_starship,$path);
 
     if (!$stmt->execute()) {
         echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
         return false;
         }
     else{
       return true;  
     }
 
     }
 
     }
     else 
     return false;
 }

 else{
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE pictures_starships SET `path` = ?  WHERE `id`= ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('si',$path,$id_starship);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
} 
}



?>